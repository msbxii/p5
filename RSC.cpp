 /*       _\|/_
          (o o)
  +----oOO-{_}-OOo---------------------------------+
  | FILE:    RSC.cpp                               |
  | AUTHOR:  Eric Young (emy16)                    |
  | DATE:    5 DEC 2011                            |
  | PROJECT: EECS325 PROJ5                         |
  | DESC:                                          |
  |   Definitions for RSCserver, RSCclient,        |
  |   and related methods.  Pretty much everything |
  |   but the ncurses UI for ./proj5.              |
  +-----------------------------------------------*/


#include <cstdlib>
#include <unistd.h>
#include <new>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <netdb.h>


#include "RSC.h"
#include "passfile.h"

#define ANON_STR "anon"
#define DEF_ROOM_STR "default"



#define NAMECMD "\\name"
#define ROOMCMD "\\room"
#define BCSTCMD "\\bcst"
#define WHOCMD "\\who"
#define WHISPERCMD "\\whisper"
#define LOGINCMD "\\login"
#define SETPWDCMD "\\setpwd"

 /// possible communication types
enum msgTypes {
	ACK = 101, NACK = 102, USER = 103, SETPWD = 104, PWDREQD = 105,
	ROOM = 106, SAY = 107, BCST = 108, DISCON = 109, SDIE = 110, WHO = 111, HOWMANY = 112,
	SAID = 113, WHISPER = 114, STATE = 115, GETPWD = 116, LOGIN = 117, GOODBYE = 118,
	USERNOTIFY = 119, JOINNOTIFY = 120, LEAVENOTIFY = 121,
	WHORESP = 122
};



/**
 * server_ini:
 * @serverinfo:	a pointer to server info structure
 * @port: 	the port for the server to listen on
 *
 * sets the correct values in a sockaddr_in structure for
 * a server to listen on a specified port, returns the socket
 * for accept()ing connections.
 */
fd_t server_ini(sockaddr_in *serverinfo, int port) {
	fd_t ret_socket = 0;

	if ((ret_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		fprintf(stderr, "[init] Could not get socket from system.\n");
		return -1;
	}

	int y = 1;
	if (setsockopt(ret_socket, SOL_SOCKET, SO_REUSEADDR, &y, sizeof(int)) < 0) {

		fprintf(stderr, "[init] Could not set socket options.\n");
		return -1;
	}

	memset(serverinfo, '\0', sizeof(sockaddr_in));
	serverinfo->sin_family = AF_INET;
	serverinfo->sin_port = htons( port );
	serverinfo->sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(ret_socket, (sockaddr *)serverinfo, sizeof(sockaddr)) < 0) {
		fprintf(stderr, "[init] Could not bind to server socket.\n");
		return -1;
	}

	if (listen(ret_socket, RSC_MAX_BACKLOG) < 0) {
		fprintf(stderr, "[init] could not begin listen() on server socket.\n");
		return -1;
	}

	printf("[init] listening on port %d\n", port);

	return ret_socket;
}


bool client::setPwd(const char* pwd) {
	if (logged_in) {
		Passkeeper::set(name, pwd);
		return true;
	} else {
		// if not logged in, see if there's already a pass
		// note: name change should prevent this
		int s = Passkeeper::auth(name, pwd);
		bool ret = false;
		switch (s) {
			// nope, give him a new pass
			case E_NOPASS:
				Passkeeper::newuser(name, pwd);
				ret = true;
				break;
			// same as what he's setting, just ack
			case AUTH:
				ret = true;
				break;
			// this shouldn't happen... 
			case E_UNAUTH:
			default:
				break;
		}
		return ret;
	}
}

bool client::login(const char* pwd) {
	int s = Passkeeper::auth(pending_name, pwd);
	bool ret = false;
	switch (s) {
		case AUTH:
			ret = true;
			logged_in = true;
			free(name);
			name = pending_name;
			pending_name = NULL;
			break;
		case E_UNAUTH:
		case E_NOPASS:
		default:
			break;
	}
	return ret;
}

bool client::setName(const char* n) {
	int s = Passkeeper::auth(n, "");
	switch(s) {
		case AUTH:
			//huh?
		case E_NOPASS:

			if (name_allocd) {
				// is it free or delete
				// for strdup'd? free() is safe
				free(name);
			}
			name = strdup(n);
			name_allocd = true;
			return true;

		case E_UNAUTH:
		default:
			return false;
	}
}

bool client::inRoom(const char* r) {
	if (!strcmp(r, room)) {
		return true; 
	} else {
		return false;
	}
}

int client::totalInRoom(const char* r) {
	int ret = 0;
	if (r == NULL) return ret;
	if (inRoom(r)) {
		ret = 1;
	}
	if (this->next == NULL) {
		return ret;
	} else {
		return ret + this->next->totalInRoom(r);
	}
}

void client::setRoom(const char* r) {
	if (room_allocd) {
		free(room);
	}
	room_allocd = true;
	room = strdup(r);
}

client* client::find(const fd_t socket) {
	if (this->socket == socket) {
		return this;
	} else {
		if (this->next != NULL) {
			return this->next->find(socket);
		} else {
			return NULL;
		}
	}
		
}

client* client::find(char* name) {
	if (!strcmp(name, this->name)) {
		return this;
	} else {
		if (this->next != NULL) {
			return this->next->find(name);
		} else {
			return NULL;
		}
	}
		
}

void client::setChild(client* c) {
	if (next != NULL) {
		next->setChild(c);
	} else {
		next = c;
	}
}

client* client::remove(client* curr, fd_t socket) {
	if (curr == NULL) return NULL;
	if (curr->socket == socket) {
		client* temp = curr->next;
		delete curr;
		return temp;
	}
	curr->next = remove(curr->next, socket);
	return curr;
}

void client::send_this(const char* message) {
	if (write(this->socket, message, strlen(message)+1 /* get the null char */) < 0) { 
		fprintf(stderr, "Could not send to client %s\n", this->name);
	}
}

void client::sendTo(char* room, char* message) {

	if (room == NULL || !strcmp(this->room, room)) {
		send_this(message);
	}
	if (this->next != NULL) {
		this->next->sendTo(room, message);
	}
}

client::client(const fd_t socket) {
	this->socket = socket;
	this->next = NULL;
	this->name = strdup(ANON_STR);
	this->name_allocd = true;
	this->room = strdup(DEF_ROOM_STR);
	this->room_allocd = true;
	this->pending_name = NULL;
	this->logged_in = false;
}

char* trim(char* buffer) { // cuts off a string at the first newline
	for (unsigned int i = 0; i < strlen(buffer); i++) {
		if (buffer[i] == '\n' || buffer[i] == '\r') {
			buffer[i] = '\0';
			return buffer;
		}
	}
	return buffer;
}

client::~client() {
	if (name_allocd)
		free(name);
	if (room_allocd)
		free(room);
}

class RSCencoder {
private:
public:
	static void bcast(char* buffer, const message* m) {
		sprintf(buffer, "%d BCST %s %s\n", BCST, m->name, m->content);
	}

	static void ack(char* buffer) {
		sprintf(buffer, "%d ACK\n", ACK);
	}
	
	static void ack(char* buffer, bool which) {
		sprintf(buffer, "%d %s\n", (which) ? ACK : NACK,
				(which) ? "ACK" : "NACK" );
	}

	static void askwho(char* buffer) {
		sprintf(buffer, "%d WHO\n", WHO);
	}

	static void usernotify(char* buffer, char* prev, char* curr) {
		sprintf(buffer, "%d USERNOTIFY %s %s\n", USERNOTIFY, prev, curr);
	}

	static void leavenotify(char* buffer, char* name) {
		sprintf(buffer, "%d LEAVENOTIFY %s\n", LEAVENOTIFY, name);
	}

	static void joinnotify(char* buffer, char* name) {
		sprintf(buffer, "%d JOINNOTIFY %s\n", JOINNOTIFY, name);
	}

	static void whisper(char* buffer, const message* m) {
		sprintf(buffer, "%d WHISPER %s %s", WHISPER, trim(m->name), trim(m->content));
	}

	// who: a list of people in the current room
	static void who(char* buffer, client* list, const client* m) {
		int pos = sprintf(buffer, "%d WHORESP", WHORESP);
		client* t = list;
		while( t != null) {
			if (t->inRoom(m->room)) {
				// assemble names of people in said room
				pos += sprintf(buffer+pos, " %s", t->name);
			}
			t = t->next;
		}
		sprintf(buffer+pos, "\n");
	}

	static void passreqd(char* buffer) {
		sprintf(buffer, "%d PWDREQD\n", PWDREQD);
	}

	static void say(char* buffer, const message* m) {
		sprintf(buffer, "%d SAY %s %s\n", SAY, trim(m->name), trim(m->content));
	}

	static void howmany(char* buffer, int tot) {
		sprintf(buffer, "%d HOWMANY %d\n", HOWMANY, tot);
	}

	static void state(char* buffer, client* c) {
		sprintf(buffer, "%d STATE\n", STATE);
	}
	
	static void stateask(char* buffer) {
		sprintf(buffer, "%d STATE\n", STATE);
	}
};

int strfind_n(const char* buff, char delim, int count) {
	int found = 0;
	for(unsigned int i = 0; i < strlen(buff); i++) {
		if (buff[i] == delim) {
			found++;
			if (found == count) {
				return i;
			}
		}
	}
	return -1;
}


class RSCparser {
public:
	static void parse(const char* buff, message* m, client* c) {
		if (buff == NULL) return;
		m->type = atoi(buff);
		// int sp_pos = strfind_n(buff, ' ', 1);
		int sp_pos2 = strfind_n(buff, ' ', 2);

		char* temp = strdup(buff);
		char* tok = strtok(temp, " ");
		// I forgot about strtok when I wrote the first few
		// but they work and I don't have time to change them
		// sorry for the inconsistency 
		switch (m->type) 
		{
			case BCST:
			case SAY: // NUM SAY <MSG>
			{
				m->name = c->name;
				if (m->content != NULL) {
					free (m->content);
				}
				m->content = strdup(buff + sp_pos2 + 1);
				// trim(m->content);
				break;
			}

			case WHISPER: // <num> WHISPER <to> <msg>
			{
				m->name = c->name;
				if (tok == NULL) {
					m->type = 0;
					break;
				}
				strtok(NULL, " \n"); // "WHISPER" 
				tok = strtok(NULL, " \n"); // <to>
				if (tok == NULL) {
					m->type = 0;
					break;
				}
				if (m->to != NULL) {
					free(m->to);
				}
				m->to = strdup(tok);

				tok = strtok(NULL, "\n"); // <msg>
				if (m->content != NULL) {
					free(m->content);
				}

				if (tok == NULL) {
					m->content = strdup(""); // empty whisper, could be useful?
				} else {
					m->content = strdup(tok);
				}
				break;
			}

			case LOGIN:
			case SETPWD: // <NUM> SETPWD|LOGIN <passwd>
			{
				m->name = c->name;
				tok = strtok(NULL, " \n"); // "SETPWD" 
				tok = strtok(NULL, " \n"); // "<passwd>"
				if (tok == NULL) {
					m->type = 0;
				} else {
					m->pwd = strdup(tok);
				}
				break;
			}


			case USER: // NUM USER <name>
			{
				if (m->name != NULL) {
					free (m->name);
				}
				m->name = strdup(buff + sp_pos2 + 1);
				trim (m->name);
				break;
			}

			case ROOM: // NUM ROOM <room>
			{
				m->name = c->name;
				if (m->room != NULL) {
					free (m->room);
				}
				m->room = strdup(buff + sp_pos2 + 1);
				trim(m->room);
				break;
			}

			default:
			{
				break;
			}
		}
	}
};


_uid_t RSCserver::hash_sockaddr(const sockaddr_in* s) {
	int ret = 0;
	ret = s->sin_addr.s_addr;
	_uid_t temp = s->sin_port;
	ret += (temp << 32);
	return ret;
}

//void RSCserver::client_add(const char* name, const fd_t socket) {
	//client *add = new client(name, socket);
	//if (clientlist == NULL) {
		//clientlist = add;
	//} else {
		//clientlist->setChild(add);
	//}
//}

void RSCserver::client_room(const fd_t socket, const char* room) {
	client *c = clientlist->find(socket);
	if (c == NULL) {
		return;
	} else {
		c->setRoom(room);
	}
}

void RSCserver::client_rm(fd_t fd) {
	clientlist = clientlist->remove(clientlist, fd);
	close(fd);
	FD_CLR(fd, &clients) ;
}

bool RSCserver::get(fd_t fd) {
	int r;
	if ((r = read(fd, this->buffer, BUFFER_SIZE)) <= 0) {
		FD_CLR(fd, &clients);
		return false;
	}
	fprintf(stderr, "Message\n");
	fprintf(stderr, "%s", buffer);
	return true;
}

int RSCserver::msgtype(const char* msg) {
	int t = atoi(msg);
	return t;
}

void RSCserver::handle(fd_t socket) {
	char* msg = buffer;
	client* c = clientlist->find(socket);
	message* m = &msgbuffer;
	memset(m, 0, sizeof(message));
	RSCparser::parse(msg, m, c);
	switch(m->type) {
		case BCST:
		{
			RSCencoder::bcast(auxbuffer, m);
			clientlist->sendTo(NULL, auxbuffer);
			break;
		}

		case DISCON:
		{
			RSCencoder::ack(auxbuffer);
			c->send_this(auxbuffer);
			client_rm(socket);
			printf("[server %s] client %s disconnected\n", 
					timestamp(),
					c->name);
			break;
		}

		case HOWMANY:
		{
			int total = clientlist->totalInRoom(c->room);
			RSCencoder::howmany(auxbuffer, total);
			c->send_this(auxbuffer);
			break;
		}

		case USER:
		{

			client* check = clientlist->find(m->name);
			if (check != NULL) {
				// name in use- nack
				RSCencoder::ack(auxbuffer, false);
				c->send_this(auxbuffer);
				break;
			}

			char* old = strdup(c->name);
			bool yn = c->setName(m->name); // checks for password necessary

			if (yn) {
				RSCencoder::ack(auxbuffer, yn);
				c->send_this(auxbuffer);

				printf("[server %s] Client %s name changed to %s\n", timestamp(), old, m->name);
				// tell people
				RSCencoder::usernotify(auxbuffer, old, m->name);
				clientlist->sendTo(c->room, auxbuffer);
			} else {
				RSCencoder::passreqd(auxbuffer);
				c->send_this(auxbuffer);
				c->pending_name = strdup(m->name);
			}

			free(old);
			break;
		}

		case WHO:
		{
			RSCencoder::who(auxbuffer, clientlist, c);
			c->send_this(auxbuffer);
			break;
		}

		case STATE:
		{
			RSCencoder::state(auxbuffer, c);
			c->send_this(auxbuffer);
			break;
		}

		case LOGIN:
		{
			if (c->pending_name == NULL) {
				RSCencoder::ack(auxbuffer, false);
				c->send_this(auxbuffer);
				break;
			}
			bool yn = c->login(m->pwd);
			RSCencoder::ack(auxbuffer,yn);
			c->send_this(auxbuffer);
			break;
		}

		case SETPWD:
		{
			bool yn = c->setPwd(m->pwd);
			if (yn) {
				RSCencoder::ack(auxbuffer, yn);
			} else {
				RSCencoder::passreqd(auxbuffer);
			}
			c->send_this(auxbuffer);
			break;
		}

		case SAY:
		{
			RSCencoder::say(auxbuffer, m);
			clientlist->sendTo(c->room, auxbuffer);
			break;
		}

		case ROOM:
		{
			char* old = strdup(c->room);

			c->setRoom(m->room);
			RSCencoder::ack(auxbuffer);
			c->send_this(auxbuffer);

			RSCencoder::leavenotify(auxbuffer, c->name);
			clientlist->sendTo(old, auxbuffer);

			RSCencoder::joinnotify(auxbuffer, c->name);
			clientlist->sendTo(c->room, auxbuffer);

			free(old);
			break;
		}

		case WHISPER:
		{
			client* to = clientlist->find(m->to);
			if (to == NULL) {
				RSCencoder::ack(auxbuffer, false);
				c->send_this(auxbuffer);
				break;
			}

			RSCencoder::whisper(auxbuffer, m);
			to->send_this(auxbuffer);

			RSCencoder::ack(auxbuffer);
			c->send_this(auxbuffer);
			break;
		}
		//case SAID:
		//case PWDREQD:
		//case ACK:
		//case NACK: /* are all nack'ed for now */
		default:
		{
			RSCencoder::ack(auxbuffer, false);
			c->send_this(auxbuffer);
			break;
		}
		
	}

}

RSCserver::RSCserver() {
	buffer = new char[BUFFER_SIZE];
	if (buffer == NULL) {
		fprintf(stderr, "Out of memory\n");
		exit(1);
	}
	clientlist = NULL;
	buffer_allocd = true;
	conneted_clients = 0;
	FD_ZERO(&clients);
}

RSCserver::~RSCserver() {
	if (buffer_allocd) {
		delete [] buffer;
	}
}


void RSCserver::setPort(int port) {
	this->port = port;
}

static char timebuffer[AUX_BUFFER_SIZE];
char* RSCserver::timestamp() { 
	time_t timeptr = time(NULL);
	struct tm* t = localtime(&timeptr);
	sprintf(timebuffer, "%02d:%02d", t->tm_hour, t->tm_min);
	return timebuffer;
}

void RSCserver::run() {
	// TODO: thread this
	// bind to socket
	sockaddr_in us;
	sockaddr_in them;
	server_socket = server_ini(&us, port); 
	printf("[server] initialized. listening...\n");
	fd_t max_socket = server_socket;

	fd_set temp_fds;
	FD_SET(server_socket, &clients);
	// TODO: install sig handler
	while(true) {
		temp_fds = clients;

		if (select(max_socket+1, &temp_fds, NULL, NULL, NULL) == -1) {
			fprintf(stderr, "[server] could not select()\n");
			return;
		}

		for(int i = 0; i <= max_socket; i++) {
			if (FD_ISSET(i, &temp_fds)) {
				// we have data from a client
				if (i == server_socket) {
					// new client connection!
					printf("[server %s] new client\n", timestamp());
					int newfd, slen = sizeof(sockaddr_in);

					if ((newfd = accept(i, (sockaddr*) &them, 
									(socklen_t*) &slen)) < 0 ) {
						fprintf(stderr, "[server %s] accept call failed\n", timestamp());
						return;
					}

					FD_SET(newfd, &clients);
					if (newfd > max_socket) {
						max_socket = newfd;
					}
					client* add = new client(newfd);
					if (clientlist == NULL) {
						clientlist = add;
					} else {
						clientlist->setChild(add);
					}

					RSCencoder::joinnotify(auxbuffer, add->name);
					clientlist->sendTo(add->room, auxbuffer);

				} else {
					// existing client 
					if (get(i))
						// successful get
						handle(i);
					else { // error or closed socket

						// log message
						client* c = clientlist->find(i);
						printf("[server %s] client %s disconnected\n", 
								timestamp(),
								c->name);
						 // notify members of the room
						RSCencoder::leavenotify(auxbuffer, c->name);
						char* room = strdup(c->room);

						// untrack
						clientlist = client::remove(clientlist, i);
						if (clientlist != NULL) {
							clientlist->sendTo(room, auxbuffer);
						}
					}
				}
			}
		}
	}
} // what a great diagonal of brackets!


/*       _\|/_
         (o o)
 +----oOO-{_}-OOo----+
 |  RSC CLIENT       |
 +-------------------*/

class RSCClientParser {
public:
	// parses the commands from the user's input box
	static bool parse(char* line, message* m) {
		if (!strlen(line)) {
			return false;
		}
		m->type = 0;
		if ( ! strncmp(line, "\\", 1)) {
			char * temp = strtok(line, " \n");
			if (temp == NULL) {
				m->type = 0;
				return false;
			}
			// command found

			if (!strcmp(temp, WHISPERCMD)) { // "\whisper <name> <msg >
				m->type = WHISPER;

				temp = strtok(NULL, " \n"); // <name>
				if (temp != NULL) {
					m->to = strdup(temp);
				} else {
					m->type = 0;
					return false;
				}

				temp = strtok(NULL, "\n");
				if (temp != NULL) {
					m->content = strdup(temp);
				} else {
					m->content = strdup("");
				}
			} else if (!strcmp(temp, NAMECMD)) { // "\name <name> "
				// SETNAME
				m->type = USER;
				temp = strtok(NULL, " \n");
				if (temp != NULL) {
					m->name = strdup(temp);
				} else {
					m->type = 0;
					return false;
				}
			} else if (!strcmp(temp, ROOMCMD)) { // "\room <room> "

				m->type = ROOM;
				temp = strtok(NULL, " \n");
				if (temp != NULL) {
					m->room = strdup(temp);
				} else {
					m->type = 0;
					return false;
				}
			} else if (!strcmp(temp, BCSTCMD)) {

				m->type = BCST;
				temp = strtok(NULL, "\n");
				if (temp != NULL) {
					m->content = strdup(temp);
				} else {
					m->type = 0;
					return false;
				}
			} else if (!strcmp(temp, LOGINCMD)){ // "\login <pass>"
				m->type = LOGIN;
				temp = strtok(NULL, " \n");
				if (temp == NULL) {
					m->type = 0;
					return false;
				}
				m->pwd = strdup(temp);
				
			} else if (!strcmp(temp, SETPWDCMD)) { // "\setpwd <pass>"
				m->type = SETPWD;
				temp = strtok(NULL, " \n");
				if (temp == NULL) {
					m->type = 0;
					return false;
				}
				m->pwd = strdup(temp);

			} else if (!strcmp(temp, WHOCMD)) {

				m->type = WHO;

			} else {
				m->type = 0;
				return false;
			}
			return true;

		} else {
			m->type = SAY;
			m->content = strdup(line);
			return true;
		}
	}

	// parses messages from the server
	static void parse_incoming(char* msg, message* m) {
		char* curr = strtok(msg, " \n");
		// get the message type ID number
		m->type = atoi(curr);

		switch(m->type) {
			case WHORESP: // <num> WHORESP <names>
			{
				strtok(NULL, " ");
				curr = strtok(NULL, "\n");
				if (curr == NULL) {
					m->type = 0;
					break;
				}
				m->content = strdup(curr);
				break;
			}

			case JOINNOTIFY: // <num> JOINNOTIFY <name>
			case LEAVENOTIFY: // <num> LEAVENOTIFY <name>
			{
				strtok(NULL, " \n");

				// get the next token, should be name
				curr = strtok(NULL, " \n");

				if (curr == NULL) {
					// no name given
					m->type = 0;
					break;
				}
				m->name = strdup(curr);
				break;
			}

			case USERNOTIFY: // <num> USERNOTIFY <old> <new>
			{
				strtok(NULL, " \n");
				curr= strtok(NULL, " \n");
				if (curr == NULL) {
					// no old name given
					m->type = 0;
					break;
				}

				m->old = strdup(curr);

				curr= strtok(NULL, " \n");
				if (curr == NULL) {
					m->type = 0;
					break;
				}
				m->name = strdup(curr);
				break;
			}


			case WHISPER: // <num> WHISPER <name> <message>
			case BCST: // <num> BCST <name> <message>
			case SAY: // <num> SAY <name> <message>
			{
				// throw away human readable token
				strtok(NULL, " \n");

				// get the next token, should be name
				curr = strtok(NULL, " \n");

				if (curr == NULL) {
					// no name given
					m->type = 0;
					break;
				}
				m->name = strdup(curr);
				 //get the rest of the string
				curr = strtok(NULL, "\n");

				if (curr == NULL) {
					m->content = strdup("");
					break;
				}
				m->content = strdup(curr);

				break;
			}
			case PWDREQD:
			case ACK:
			{
				break;
			}
			default:
			{
				m->type = 0;
				break;
			}
		}
		return;
	}
};

class RSCClientEncoder {
public:
	static void whoreq(char* buff) {
		sprintf(buff, "%d WHO\n", WHO);
	}

	static void joinRoom(char* buff, message* m) {
		sprintf(buff, "%d ROOM %s\n", ROOM, m->room);
	}
	static void setName(char* buff, message* m) {
		sprintf(buff, "%d USER %s\n", USER, m->name);
	}
	static void broadcast(char* buff, message* m) {
		sprintf(buff, "%d BCST %s\n", BCST, m->content);
	}
	static void say(char* buff, message* m) {
		sprintf(buff, "%d SAY %s\n", SAY, m->content);
	}
	static void disconnect(char* buff) {
		sprintf(buff, "%d DISCON\n", DISCON);
	}
	static void whisper(char* buff, message* m) {
		sprintf(buff, "%d WHISPER %s %s\n", WHISPER, m->to, m->content);
	}
	static void setpwd(char* buff, message* m) {
		sprintf(buff, "%d SETPWD %s\n", SETPWD, m->pwd);
	}
	static void login(char* buff, message* m) {
		sprintf(buff, "%d LOGIN %s\n", LOGIN, m->pwd);
	}
};


RSCclient::RSCclient() {
	auxbuffer = (char*) malloc(AUX_BUFFER_SIZE);
	inbuffer = (char*) malloc(BUFFER_SIZE);
	last_cmd = 0;
	connected = false;
	current_room = strdup(DEF_ROOM_STR);
	current_name = strdup(ANON_STR);
	pending_name = NULL;
	pending_room = NULL;
	echo = true;
}

RSCclient::~RSCclient() {
	free(auxbuffer);
	free(inbuffer);
	free(current_room);
	free(current_name);
	if (pending_name != NULL) {
		free(pending_name);
	}
	if (pending_room != NULL) {
		free(pending_room);
	}
}

bool RSCclient::init(char* hostname, int port) {
	pthread_mutex_init(&objmutex, NULL);
	sockaddr_in server;
	hostent* server_host;
	server_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (server_socket < 0) {
		//error 
		return false;
	}

	server_host = gethostbyname(hostname);
	if (server_host == NULL) {
		return false;
	}

	memset(&server, 0, sizeof(sockaddr_in));
	server.sin_family = AF_INET;
	memcpy(&server.sin_addr.s_addr, server_host->h_addr, server_host->h_length);
	server.sin_port = htons(port);

	if (connect(server_socket, (const sockaddr *) &server, sizeof(server)) < 0) {
		return false;
	}
	connected = true;
	return true;
}

bool RSCclient::send(char* msg) {

	if (!connected) {
		return false;
	}
	int written = write(server_socket, msg, strlen(msg) + 1 /* null char */);
	if (written < 0) {
		return false;
	}
	return true;
}

void RSCclient::disconnect() {
	close(server_socket);
}

// send messages to the server
bool RSCclient::command(char* msgbuff) {

	pthread_mutex_lock(&objmutex);

	message message_self; 
	memset(&message_self, 0, sizeof(message_self));
	memset(auxbuffer, 0, AUX_BUFFER_SIZE);
	RSCClientParser::parse(msgbuff, &message_self);
	last_cmd = message_self.type;
	bool ret = false;
	switch(message_self.type) {
		case SAY:
		{
			RSCClientEncoder::say(auxbuffer, &message_self);
			ret =  send(auxbuffer);
			break;
		}

		case BCST:
		{
			RSCClientEncoder::broadcast(auxbuffer, &message_self);
			ret =  send(auxbuffer);
			break;
		}

		case WHO:
		{
			RSCClientEncoder::whoreq(auxbuffer);
			ret =  send(auxbuffer);
			break;
		}

		case USER:
		{
			RSCClientEncoder::setName(auxbuffer, &message_self);
			ret = send(auxbuffer);
			if (pending_name != NULL) {
				free(pending_name);
			}
			pending_name = strdup(message_self.name);
			break;
		}

		case ROOM:
		{
			RSCClientEncoder::joinRoom(auxbuffer, &message_self);
			ret = send(auxbuffer);
			if (pending_room != NULL) {
				free(pending_room);
			}
			pending_room = strdup(message_self.room);
			break;
		}

		case WHISPER:
		{
			RSCClientEncoder::whisper(auxbuffer, &message_self);
			ret = send(auxbuffer);
			break;
		}

		case LOGIN:
		{
			RSCClientEncoder::login(auxbuffer, &message_self);
			ret = send(auxbuffer);
			break;
		}

		case SETPWD:
		{
			RSCClientEncoder::setpwd(auxbuffer, &message_self);
			ret = send(auxbuffer);
			break;
		}

		default:
		{
			break;
		}
	}
	if (message_self.name != NULL) {
		free (message_self.name);
	}
	if (message_self.content != NULL) {
		free (message_self.content);
	}
	if (message_self.pwd != NULL) {
		free (message_self.pwd);
	}
	if (message_self.room != NULL) {
		free (message_self.room);
	}
	if (message_self.to != NULL) {
		free (message_self.to);
	}
	pthread_mutex_unlock(&objmutex);
	return ret;

}

// process messages from the server
bool RSCclient::run(char* msgbuff) {

	*msgbuff = '\0';
	fd_set server;
	message m;
	memset(&m, 0, sizeof(m));
	FD_ZERO(&server);
	FD_SET(server_socket, &server);
	char inbuffer_l[BUFFER_SIZE];

	if (select(server_socket + 1, &server, NULL, NULL, NULL) < 1) {
		return false;
	}

	pthread_mutex_lock(&objmutex);

	// sockets magic!! 
	int r = read(server_socket, inbuffer_l, BUFFER_SIZE);
	if (r <= 0) {
		return false;
	}
	// to give to clientparser
	char* pass = (char*) malloc(BUFFER_SIZE);
	strcpy(pass, inbuffer_l);

	RSCClientParser::parse_incoming(pass, &m);

	free(pass); // haha! I need one! that would be nice.

	
	switch (m.type) {
		// this switch assembles messages for display by the caller
		case SAY:
		{
			if (echo) {
				sprintf(msgbuff, "[%s %s] %s\n", m.name, RSCserver::timestamp(), m.content);
			} else {
				sprintf(msgbuff, "%s", "");
			}
			break;
		}
		case WHORESP:
		{
			sprintf(msgbuff, "[SERVER %s] in this room: %s\n", RSCserver::timestamp(), m.content);
			break;
		}
		case BCST:
		{
			if (echo) {
				sprintf(msgbuff, "[%s BCST %s] %s\n", m.name, RSCserver::timestamp(), m.content);
			} else {
				sprintf(msgbuff, "%s", "");
			}
			break;
		}
		case USERNOTIFY:
		{
			sprintf(msgbuff, "[SERVER %s] %s changed name to %s\n", RSCserver::timestamp(), m.old, m.name);
			break;
		}
		case LEAVENOTIFY:
		{
			sprintf(msgbuff, "[SERVER %s] %s has left the room.\n", RSCserver::timestamp(), m.name);
			break;
		}
		case JOINNOTIFY:
		{
			sprintf(msgbuff, "[SERVER %s] %s has joined the room.\n", RSCserver::timestamp(), m.name);
			break;
		}
		case ACK:
		{
			if (last_cmd == ROOM) {
				sprintf(msgbuff, "room changed to %s\n", pending_room);
				if (current_room != NULL) {
					free(current_room);
					current_room = pending_room;
					pending_room = NULL;
				}
			} else if (last_cmd == USER) {
				sprintf(msgbuff, "name changed to %s\n", pending_name);
				if (current_name != NULL) {
					free(current_name);
					current_name = pending_name;
					pending_name = NULL;
				}
			} else if (last_cmd == LOGIN) {
				sprintf(msgbuff, "[ login okay ]\n");
			} else if (last_cmd == SETPWD) {
				sprintf(msgbuff, "[ password change okay ]\n");
			} else {
				sprintf(msgbuff, "%s", "");
			}
			last_cmd = 0;
			break;
		}
		case WHISPER:
		{
			sprintf(msgbuff, "[%s %s whispered to you:] %s\n", m.name, RSCserver::timestamp(), m.content);
			break;
		}
		case NACK:
		{
			if (last_cmd == ROOM) {
				sprintf(msgbuff, "![room change failed]\n");
				free(pending_room);
				pending_room = NULL;

			} else if (last_cmd == USER) {
				sprintf(msgbuff, "![name change failed]\n");
				free(pending_name);
				pending_name = NULL;

			} else if (last_cmd == LOGIN) {
				sprintf(msgbuff, "![ login failed ]\n");
				if (pending_name != NULL) {
					free(pending_name);
				}

			} else if (last_cmd == SETPWD) {
				sprintf(msgbuff, "![ password change failed ]\n");

			} else {
				sprintf(msgbuff, "%s",  "");
			}
			break;
		}
		case PWDREQD:
		{
			if (last_cmd == USER) {
				sprintf(msgbuff, "![please send password for username %s]\n", pending_name);
			} else if (last_cmd == ROOM) {
				sprintf(msgbuff, "![please send password for room %s]\n", pending_room);
			} else {
				sprintf(msgbuff, "![ password required ]\n");
			}
			break;
		}
		default:
		{
			sprintf(msgbuff, "%s", "");
			break;
		}
	}
	if (m.name != NULL) {
		free (m.name);
	}
	if (m.old != NULL) {
		free (m.old);
	}
	if (m.content != NULL) {
		free (m.content);
	}
	if (m.pwd != NULL) {
		free (m.pwd);
	}
	if (m.room != NULL) {
		free (m.room);
	}
	last_cmd = 0;
	pthread_mutex_unlock(&objmutex);
	return true;
}






// vim: ts=4;sw=4
