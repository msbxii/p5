
#include "passfile.h"
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>



#define FIELD_SIZE 20



struct unpw_t {
	char un[FIELD_SIZE + 1];
	char pw[FIELD_SIZE + 1];
};

bool unpw_eq(struct unpw_t* first, const char* user) {
	if (strncmp(first->un, user, FIELD_SIZE)) {
		return false;
	} else {
		return true;
	}
}

unpw_t* new_unpw(const char* user, const char* pass) {
	unpw_t* ret = (unpw_t*) malloc(sizeof(unpw_t));

	strncpy(ret->un, user, FIELD_SIZE);
	ret->un[FIELD_SIZE] = '\0';

	strncpy(ret->pw, pass, FIELD_SIZE);
	ret->pw[FIELD_SIZE] = '\0';
	return ret;
}

int Passkeeper::auth(const char* user, const char* pass) {
	char pbuff[FIELD_SIZE + 1];
	
	if (!lookup(user, pbuff)) {
		return E_NOPASS;
	}

	if (!strcmp(pass, pbuff)) {
		return AUTH;
	} else {
		return E_UNAUTH;
	}
}

bool Passkeeper::set(const char* user, const char* pass) {

	long int pos = getpos(user);

	if (pos < 0) {
		return false;
	}

	FILE* passfile = fopen(PASSFILE, "rb+");
	unpw_t* update = new_unpw(user, pass);

	fseek(passfile, pos, SEEK_SET);
	fwrite(update, sizeof(unpw_t), 1, passfile);

	fclose(passfile);
	free(update);
	return true;
}

bool Passkeeper::newuser(const char* user, const char* pass) {
	FILE* passfile = fopen(PASSFILE, "ab+");

	if (passfile == NULL) {
		fprintf(stderr, "[Passkeeper] could not open passfile: %s\n", strerror(errno));
		return false;
	}

	unpw_t* update = new_unpw(user, pass);
	fwrite(update, sizeof(unpw_t), 1, passfile);

	fclose(passfile);
	free(update);

	return true;
}

bool Passkeeper::lookup(const char* user, char* passbuffer) {
	unpw_t current;
	FILE* passfile = fopen(PASSFILE, "r");
	if (passfile == NULL) {
		return false;
	}
	while( !feof(passfile)) {
		if (fread(&current, sizeof(current), 1, passfile) != 1) {
			return false;
		}
		if (unpw_eq(&current, user)) {
			strncpy(passbuffer, current.pw, FIELD_SIZE + 1);
			return true;
		}
	}
	return false;
}

long int Passkeeper::getpos(const char* user) {
	unpw_t current;
	FILE* passfile = fopen(PASSFILE, "r");
	if (passfile == NULL) {
		return -1;
	}
	long int i = 0;
	while( !feof(passfile)) {
		if (fread(&current, sizeof(current), 1, passfile) != 1) {
			return -1;
		}
		if (unpw_eq(&current, user)) {
			return i;
		}
		i += sizeof(current);
	}
	return -1;
}
	
