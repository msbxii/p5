/*       _\|/_
         (o o)
 +----oOO-{_}-OOo---------------------------------+
 | FILE passfile.h                                |
 | AUTHOR Eric Young (emy16)                      |
 | DATE 10 DEC 2011                               |
 | DESC a class designed for authenticating users |
 |      for RSC.                                  |
 |      For now, passwords are being stored       |
 |      unencrypted because they're not being     |
 |      encrtyped for transmission.  It doesn't   |
 |      make much sense to hash something to disk |
 +-----------------------------------------------*/


#ifndef _PASSFILE_H_
#define _PASSFILE_H_


#define PASSFILE ".rsc.shadow"

enum {E_UNAUTH = -1, E_NOPASS, AUTH}; // return values for auth()

class Passkeeper {
private:

	//static void decrypt(const char* pass, char* out);
	//static void encrypt(const char* pass, char* out);

	static bool lookup(const char* user, char* passbuffer);

	static long int getpos(const char* user);

	// char* passfile;

public:
	// authenticate a user. 
	// returns: -1 on unauth,
	//          0 on nopass,
	//          1 on success
	static int auth(const char* user, const char* pass);
	
	// sets a user's pass
	// returns:
	//    false for no user,
	//    true for success
	static bool set(const char* user, const char* pass);

	// makes a new user- to be used after auth() returns 0
	// returns false for failure
	static bool newuser(const char* user, const char* pass);

};


#endif /* _PASSFILE_H_ */
