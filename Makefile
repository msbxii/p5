CC=g++
CXXFLAGS = -Wall -g
LDFLAGS=-lncurses -lpthread -pthread

.PHONY: all

all: proj5d proj5

proj5d: proj5d.o RSC.o passfile.o

proj5: proj5.o RSC.o passfile.o

test: test.o passfile.o

clean:
	$(RM) *.o proj5 proj5d test
