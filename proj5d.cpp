#include "RSC.h"
#include <cstdio>
#include <cstdlib>



int main( int argc, char** argv ) {
	int portno = DEFAULT_PORT;
	
	if (argc > 1) {
		int temp_port = atoi(argv[1]);
		if (temp_port <= 0 || temp_port > UINT16_MAX) {
			fprintf(stderr, "Inavlid port number given.  Using default..\n");
		} else {
			portno = temp_port;
		}
	}

	RSCserver* server = new RSCserver();
	server->setPort(portno);
	server->run();
}

