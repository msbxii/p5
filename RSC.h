 /*       _\|/_
          (o o)
  +----oOO-{_}-OOo---------------------------------+
  | FILE:    RSC.h                                 |
  | AUTHOR:  Eric Young (emy16)                    |
  | DATE:    5 DEC 2011                            |
  | PROJECT: EECS325 PROJ5                         |
  | DESC:                                          |
  |   Declarations for RSCserver, RSCclient,       |
  |   and related methods.  Pretty much everything |
  |   but the ncurses UI for ./proj5.              |
  +-----------------------------------------------*/

#ifndef __RSC_H_
#define __RSC_H_

#include <stdint.h>
#include <netinet/in.h>
#include <sys/select.h>

#define DEFAULT_PORT 14324
#define BUFFER_SIZE 8192
#define AUX_BUFFER_SIZE 1024
#define UINT16_MAX 0xffff

#define null NULL

#define RSC_MAX_BACKLOG 5 //maximum number of simultaenous pending connections- see listen(2)

typedef uint64_t _uid_t;
typedef int fd_t;

// generally holds information about a message/command. usage is not as
// standard as it should be.
class message {
    public:
	int type;
	char* content;
	char* name;
	char* room;
	char* pwd;
	char* to;
	// when recieving a USERNOTIFY, this holds the first token.
	char* old;
};

// structure representing a connected client.
class client {

public:
	char* name;
	bool name_allocd; // always true?
	char* room;
	bool room_allocd; // ditto
	fd_t socket; // file descriptor for the client's socket
	client* next; // it's actually a linked list!
	bool logged_in;

	char* pending_name; // for password auth

	bool setPwd(const char* pwd); // sets the clients password

	bool login(const char* pwd); // attempts login. false for bad pw

	bool setName(const char* n); // sets name.  false for in-use

	bool inRoom(const char* r); // sets room. Noauth

	int totalInRoom(const char* r); // counts total in room. call on listhead.

	void setRoom(const char* r); // sets room. never fails

	client* find(const fd_t socket); // returns object with socket `socket', NULL on dne

	client* find(char* name); // ditto

	void setChild(client* c); // inserts a client into the list, at the end

	static client* remove(client* curr, fd_t socket); // removes a client, 
	// returns new listhead. pass listhead as curr,
	// set listhead to the return value.

	void send_this(const char* message); // raw send of `message'

	void sendTo(char* room, char* message); // if in `room' , calls send_this(message)

	client(const fd_t socket); //constructor

	~client();
};

class RSCserver {

private:
	message msgbuffer; 				/* buffer for message structs- avoid new() */
	fd_t server_socket; 			/* the server's listen socket */
	int conneted_clients; 			/* the number of connected clients */
	int port; 						/* server's listen port */
	char* buffer; 					/* buffer for holding messages; 8192? */
	char auxbuffer[AUX_BUFFER_SIZE]; /* aux buffer for encoding messages */
	bool buffer_allocd; 			/* whether the buffer has been alloc'd */
	fd_set clients; 				/* set of connected clients' fd's */

	
	void client_add(const char* name, const fd_t socket); // notused

	_uid_t hash_sockaddr(const sockaddr_in* s); //not used

	void client_room(const fd_t socket, const char* room); //not used

	void client_rm(fd_t fd); //not used

	bool get(fd_t fd); // puts data from `fd' into this->buffer

	int msgtype(const char* msg); // don't think this is ever called

	void handle(fd_t socket); // calls get(socket), switches on the message type


	client* clientlist; // the main set of client objects.

public: 
	 RSCserver();
	~RSCserver();

	static char* timestamp() ; // returns a static buffer containing time timestamp
	// in hr:min format.  

	void setPort(int port); // sets the server's listen port.  Call before run().

	void run() ; // starts the server listening.  Blocks forever.
};


class RSCclient {

private:

	pthread_mutex_t objmutex; // to prevent threads from 
								// screwing up internal data structures

	fd_t server_socket; // main connection socket to server

	bool send(char* message); // sends raw data in `message' to the server_socket

	// these are self-explanitory
	bool joinRoom(char* room) ; 
	bool broadcast(char* message);
	bool name(char* name);
	bool say(char* message);

	// for holding client commands
	char* auxbuffer;
	// for holding server messages
	char* inbuffer;

	// for name/room change, authentication
	char* current_room;
	char* pending_room;
	char* current_name;
	char* pending_name;

	int last_cmd; // for giving sensible ack messages
	bool connected; // just in case somebody calls run() or command() after
				// a bad init()

public:
	bool echo; /* whether or not messages are displayed- does not effect whispers */
	RSCclient() ;

	~RSCclient() ;

	bool run(char* msgbuff); // listens for server messages, puts a display message into msgbuff
							// for each message

	// opens a socket to `hostname' on port `port'
	bool init(char* hostname, int port);

	void disconnect(); // ends session

	void list(); // hmm.... not sure what this does

	bool command(char* msgbuff); // parses the user command in `msgbuff' and
								// communicates with server. blocks on objmutex
};


#endif /* __RSC_H_ */
