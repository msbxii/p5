

#include "passfile.h"
#include <string>
#include <stdio.h>

using namespace std;

int main() {
	string user = "mark";
	string pass = "someotherstuff";

	int ret = Passkeeper::auth(user.c_str(), pass.c_str());
	switch(ret) {
		case E_UNAUTH:
			printf("invalid password.\n");
			break;
		case E_NOPASS:
			Passkeeper::newuser(user.c_str(), pass.c_str());
			break;
		case AUTH:
			printf("successful auth\n");
			break;
		default:
			break;
	}
}
