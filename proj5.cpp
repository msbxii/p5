/*       _\|/_
         (o o)
 +----oOO-{_}-OOo--------------------------------+
 | FILE: proj5.cpp                               |
 | AUTHOR: Eric Young (emy16)                    |
 | DATE: 7 December 2011                         |
 | DESCRIPTION                                   |
 |     An ncurses-based client for communication |
 |     over RSC (really simple chat)             |
 +----------------------------------------------*/



#include <ncurses.h>
#include <string.h>
#include <malloc.h>
#include <pthread.h>
#include <string.h>
#include <cstdlib>
#include <signal.h>
#include <stdio.h>
#include <string>

#include "RSC.h"

#define BUFFSIZE 8192
#define MAX_STR_SIZE 10000

#define QUIT_CMD "\\quit"
#define ECHOCMD "\\echo"
#define NOECHOCMD "\\noecho"

using namespace std;

void* listener(void* arg);

// communication between threads
struct listenerarg {
	// so we don't draw to the window simultaneously
	// (was giving some interesting garbage to the window)
	pthread_mutex_t window_mutex;

	// holds all messages for the user
	WINDOW* textwin;
	// holds user commands
	WINDOW* inwin;
	char* buffer;
	// the client object - handles communication to the server
	RSCclient* client;
	// server hostname
	char* hostname;
	// server port
	int host_port;
	// if the program should still be running- flags quit conditions
	bool running;
};

// sets up a window with or without a border
WINDOW* u_new_win(int height, int width, int startx, int starty, bool border) {
	WINDOW* ret;
	ret = newwin(height, width, startx, starty);
	if (border) {
		wborder(ret, '|', '|', '-', '-', '+', '+', '+', '+');
	} else {
		wborder(ret, ' ',' ',' ',' ',' ',' ',' ',' ');
	}
	wrefresh(ret);

	return ret;
}

// Handles C-C- shut down ncurses because 
// it really sucks when endwin() isn't called in your term
void quitstuff(int signal) {
	endwin();
	printf("Thanks for using RSC\n");
	exit(0);
	// we don't need to call disconnect() - server sees a closed socket
	// as a clean disconnect.
}

int main(int argc, char** argv) {

	if (argc != 2 && argc != 3) {
		fprintf(stderr, "Usage: %s SERVER [PORT]\n", argv[0]);
		return 1;
	}


	WINDOW *textboxborder, *textbox, *inboxborder, *inbox;
	int x, y, width, height;

	// TODO install handler
	initscr();

	signal(SIGINT, quitstuff);
	// cbreak();
	// noecho();

	keypad(stdscr, TRUE);

	x = 0;
	y = 0;
	height = LINES - 3;
	width = COLS;
	refresh();

	// set up window structure.  the *border boxes
	// contain nothing but the border, so that the other
	// boxes may be cleared/written to without disturbing the border.
	// use of magic numbers is necessary here, sorry. it would probably
	// be good to put this in a function so the window can be resized.
	textboxborder = u_new_win(height, width, x, y, true);
	textbox = u_new_win(height - 2, width-4, x+1, y+2, false);
	inboxborder = u_new_win(3, COLS, LINES - 3, 0, true);
	inbox = u_new_win(1, COLS - 3, LINES - 2, 2, false);

	// put cursor in the input box
	wmove(inbox, 0, 0);
	// show changes
	wrefresh(inbox);

	int curwidth = COLS;
	char* in_disp_buff = (char*) malloc(BUFFSIZE);
	size_t in_disp_pos = 0;
	in_disp_buff[in_disp_pos] = '\0';

	char *in_blank_buff = (char*) malloc(curwidth - 1);
	memset(in_blank_buff, ' ', curwidth - 1);

	in_blank_buff[curwidth - 2] = '\0';

	// spawn listener: 
	listenerarg send;
	// give the listener thread access to the windows, so it can
	// move the cursor back to the input window after messages
	send.buffer = in_disp_buff;
	send.textwin = textbox;
	send.inwin = inbox;

	// to avoid draw race conditions
	pthread_mutex_init(&send.window_mutex, NULL);
	
	// connection data
	send.hostname = strdup(argv[1]);
	send.host_port = DEFAULT_PORT;
	// this flag will disconnect the listener
	send.running = true;

	if (argc == 3) {
		send.host_port = atoi(argv[2]);
		if (send.host_port <= 0 || send.host_port > UINT16_MAX) {
			endwin();
			fprintf(stderr, "invalid port number\n");
			return 1;
		}
	}
	pthread_t listener_pid;
	int err = pthread_create(&listener_pid, NULL, listener, (void *) &send);

	if (err) {
		endwin();
		printf("Could not initialize listener thread\n");
		pthread_exit(NULL);
	}


	// put a colon in the input box.  I like it.. 
	mvwprintw(inboxborder, 1, 1, ":");
	wrefresh(inboxborder);

	while (send.running && mvwgetstr(inbox, 0, 0, in_disp_buff) != ERR) {
	
		pthread_mutex_lock(&send.window_mutex);

		// remove the user's command from the box, reset cursor
		wclear(inbox);
		wrefresh(inbox);

		// check for behavioral commands- quit, echo, noecho
		if (!strncmp(in_disp_buff, QUIT_CMD, strlen(QUIT_CMD))) {
			// "\quit"
			send.running = false;
		} else if (! strncmp(in_disp_buff, NOECHOCMD, strlen(NOECHOCMD))) {
			// "\noecho"
			send.client->echo = false;
		} else if (! strncmp(in_disp_buff, ECHOCMD, strlen(ECHOCMD))) {
			// "\echo"
			send.client->echo = true;
		} else {
			// pass it on to the client
			if (! send.client->command(in_disp_buff)) {
				// not a valid command.. ignore for now
			}
		}
		memset(in_disp_buff, 0, BUFFSIZE);

		pthread_mutex_unlock(&send.window_mutex);
	}
	// pthread_join(listener_pid, NULL);
	endwin();
	exit(0);
}

void* listener(void* arg) {

	string curr_win_str = "";

	int x,y;
	listenerarg* out = (listenerarg*) arg;
	RSCclient* client = new RSCclient();
	out->client = client;
	// install sig handler? 
	//
	if (! client->init(out->hostname, out->host_port)) {

		// graceful error message
		getyx(out->inwin, y, x);
		string ret = (char*) malloc(AUX_BUFFER_SIZE);
		string insert("[ERROR] could not connect to server.");
		wprintw(out->textwin, "%s", insert.c_str());
		wmove(out->inwin, y, x);
		wrefresh(out->textwin);
		wrefresh(out->inwin);
		pthread_exit(NULL);
	}

	char clientbuffer[BUFFSIZE];
	// grab messages from server
	while(out->running && client->run(clientbuffer)) {
		pthread_mutex_lock( &out->window_mutex);
		
		// put the client's interpretation onto textwin
		getyx(out->inwin, y, x);
		wmove(out->textwin, 0, 0);
		wclear(out->textwin);
		string insert(clientbuffer);
		// keep new messages at the top
		curr_win_str = insert.append(curr_win_str);
		wprintw(out->textwin, "%s", curr_win_str.c_str());
		wrefresh(out->textwin);

		wmove(out->inwin, y, x);
		wrefresh(out->inwin);

		// Limit memory usage
		if (curr_win_str.size() > MAX_STR_SIZE) {
			curr_win_str.erase(MAX_STR_SIZE, curr_win_str.size() - MAX_STR_SIZE);
		}
		pthread_mutex_unlock(&out->window_mutex);
	}
	if (!out->running) {
		// this condition means that we're still connected
		client->disconnect();
	}
	out->running = false; // if not already false, the main thread doesn't
							// know we lost connection 
	string insert("[listener] disconnected from server."); // user will probably never see this
	wclear(out->textwin);
	wmove(out->textwin, 0,0);
	wprintw(out->textwin, "%s", insert.c_str());
	wrefresh(out->textwin);
	pthread_exit(NULL);
}
